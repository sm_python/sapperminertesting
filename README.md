﻿# README #


### Cоставление отчета об ошибках в программе "Сапер" ###

* Протестировать приложение [miner.jar](miner.jar) на ошибки
* Заполнить Форму [BugReport_Template.odt](BugReport_Template.odt) или [BugReport_Template.doc](BugReport_Template.doc) 
* Для запуска приложения нужно установить: [Java SE Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

